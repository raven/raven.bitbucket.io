const ravenData = [
	{
		id: 1,
		title: 'The night of the Sixth October',
		numberOfImages: 4,
		backgroundImage: '../assets/bg1.png',
	},
	{
		id: 2,
		title: 'The night of the Sixth November',
		numberOfImages: 5,
		backgroundImage: '../assets/bg2.png',
	},
	{
		id: 3,
		title: 'The night of the Sixth December',
		numberOfImages: 2,
		backgroundImage: '../assets/bg3.png',
	},
	{
		id: 4,
		title: 'The night of the Sixth October',
		numberOfImages: 7,
		backgroundImage: '../assets/Bg4.png',
	},
	{
		id: 5,
		title: 'The night of the Sixth October',
		numberOfImages: 3,
		backgroundImage: '../assets/Bg5.png',
	},
	{
		id: 6,
		title: 'The night of the Sixth October',
		numberOfImages: 7,
		backgroundImage: '../assets/Bg6.png',
	},
	{
		id: 7,
		title: 'The night of the Sixth October',
		numberOfImages: 7,
		backgroundImage: '../assets/Bg7.png',
	},
	{
		id: 8,
		title: 'The night of the Sixth October',
		numberOfImages: 7,
		backgroundImage: '../assets/Bg8.png',
	},
];

export default ravenData;
